# Standard Example

This example illustrates how to use the `cloud_storage` module.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| bucket\_name | The name of the bucket to save the compressed file too | `string` | n/a | yes |
| company\_prefix | the unique prefix for resource naming | `string` | n/a | yes |
| environment\_variables | Map of key value pairs to be assigned to the environment | `map(any)` | `{}` | no |
| files\_to\_exclude\_in\_source\_dir | Specify files to ignore when reading the source\_dir | `list(string)` | `[]` | no |
| function\_runtime | The runtime for the function | `string` | `"python39"` | no |
| function\_stack\_http | An object that sets `entry_point` `function_name` optionally :`timeout`, `function_description` and a map of `environment_variables` | `map(any)` | `{}` | no |
| function\_stack\_pubsub | An object that sets `entry_point` `function_name`  `pub_sub_name`, optionally: `timeout`, `function_description` and a map of `environment_variables` | `map(any)` | `{}` | no |
| labels | labels to be applied | `map(any)` | `{}` | no |
| path\_to\_function\_code | The relative path (from terraform code) to the diretory of the cloud function code | `string` | n/a | yes |
| project\_id | The project in which the function will be created | `any` | n/a | yes |
| region | The location of the function | `string` | `"australia-southeast1"` | no |
| service\_account\_email | The name of the service account to run the function | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| function\_region | The region of the function that's been created |
| http\_function\_names | The names of the http functions that's been created |
| pubsub\_function\_names | The name of the pubsub functions that's been created |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

To provision this example, run the following from within this directory:
- `terraform init` to get the plugins
- `terraform plan` to see the infrastructure plan
- `terraform apply` to apply the infrastructure build
- `terraform destroy` to destroy the built infrastructure
