/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

module "cloud_function_stack" {
  source                = "../../modules/function_stack"
  bucket_name           = var.bucket_name
  path_to_function_code = var.path_to_function_code
  company_prefix        = var.company_prefix
  project_id            = var.project_id
  function_runtime      = var.function_runtime
  service_account_email = var.service_account_email
  labels                = var.labels
  environment_variables = var.environment_variables
  function_stack_http   = var.function_stack_http
  function_stack_pubsub = var.function_stack_pubsub
}
