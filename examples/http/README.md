# Standard Example

This example illustrates how to use the `cloud_storage` module.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| available\_memory\_mb | (Optional) Memory (in MB), available to the function. Default value is 256. Possible values include 128, 256, 512, 1024, etc. | `number` | `256` | no |
| bucket\_name | The name of the bucket to save the compressed file too | `string` | n/a | yes |
| company\_prefix | the unique prefix for resource naming | `string` | n/a | yes |
| entry\_point | n/a | `string` | `"The name of the function that is the entry point to the function"` | no |
| environment\_variables | Map of key value pairs to be assigned to the environment | `map(any)` | `{}` | no |
| function\_description | The name for the function | `string` | `"A function"` | no |
| function\_name | The name of the function to be created | `string` | n/a | yes |
| function\_runtime | The runtime for the function | `string` | `"python39"` | no |
| labels | labels to be applied | `map(any)` | `{}` | no |
| path\_to\_function\_code | The relative path (from terraform code) to the diretory of the cloud function code | `string` | n/a | yes |
| project\_id | The project in which the function will be created | `any` | n/a | yes |
| region | The location of the function | `string` | `"australia-southeast1"` | no |
| service\_account\_email | The name of the service account to run the function | `string` | n/a | yes |
| timeout | Timeout (in seconds) for the function. Default value is 60 seconds. Cannot be more than 540 seconds. | `number` | `60` | no |

## Outputs

| Name | Description |
|------|-------------|
| function\_name | The name of the function that's been created |
| function\_region | The region of the function that's been created |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

To provision this example, run the following from within this directory:
- `terraform init` to get the plugins
- `terraform plan` to see the infrastructure plan
- `terraform apply` to apply the infrastructure build
- `terraform destroy` to destroy the built infrastructure
