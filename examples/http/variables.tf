variable "project_id" {
  description = "The project in which the function will be created"
}
variable "bucket_name" {
  type        = string
  description = "The name of the bucket to save the compressed file too"

}
variable "path_to_function_code" {
  type        = string
  description = "The relative path (from terraform code) to the diretory of the cloud function code"

}

variable "company_prefix" {
  type        = string
  description = "the unique prefix for resource naming"
}

variable "entry_point" {
  type    = string
  default = "The name of the function that is the entry point to the function"

}

variable "labels" {
  type        = map(any)
  description = "labels to be applied"
  default     = {}

}
variable "timeout" {
  type        = number
  default     = 60
  description = "Timeout (in seconds) for the function. Default value is 60 seconds. Cannot be more than 540 seconds."
}
variable "available_memory_mb" {
  type        = number
  default     = 256
  description = "(Optional) Memory (in MB), available to the function. Default value is 256. Possible values include 128, 256, 512, 1024, etc."

}

variable "environment_variables" {
  type        = map(any)
  description = "Map of key value pairs to be assigned to the environment"
  default     = {}

}
variable "service_account_email" {
  type        = string
  description = "The name of the service account to run the function"

}
variable "region" {
  description = "The location of the function"
  default     = "australia-southeast1"

}

variable "function_name" {
  type        = string
  description = "The name of the function to be created"

}
variable "function_description" {
  type        = string
  default     = "A function"
  description = "The name for the function"

}

variable "function_runtime" {
  type        = string
  default     = "python39"
  description = "The runtime for the function"

}
