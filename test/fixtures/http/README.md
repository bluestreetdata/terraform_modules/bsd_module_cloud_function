


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| project\_id | The project in which the buckets will be created | `any` | n/a | yes |
| service\_account\_email | The email of the serivce account that runs the function | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| function\_name | The name of the function that's been created |
| function\_region | The region of the function that's been created |
| project\_id | The ID of the project in which resources are provisioned. |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->