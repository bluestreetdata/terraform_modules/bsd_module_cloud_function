# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/archive" {
  version = "2.2.0"
  hashes = [
    "h1:CIWi5G6ob7p2wWoThRQbOB8AbmFlCzp7Ka81hR3cVp0=",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version = "4.16.0"
  hashes = [
    "h1:NvmY7SXSROwpOBaunbDeVBDyaV3w7jGLs6d9TpDZYLw=",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version = "3.1.1"
  hashes = [
    "h1:71sNUDvmiJcijsvfXpiLCz0lXIBSsEJjMxljt7hxMhw=",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.1.2"
  hashes = [
    "h1:5A5VsY5wNmOZlupUcLnIoziMPn8htSZBXbP3lI7lBEM=",
  ]
}
