/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

# provider "random" {
#   version = "~> 2.0"
# }

resource "random_string" "company_prefix" {
  length  = 3
  lower   = true
  special = false
  number  = false
  upper   = false
}

resource "random_pet" "main" {
  length    = 2
  prefix    = "pubsub"
  separator = "-"
}

resource "random_pet" "pubsub" {
  length    = 2
  separator = "-"
}

locals {
  environment_variables = { "FOO" : "BAR" }
}

module "example" {
  source                = "../../../examples/pubsub"
  bucket_name           = "${var.project_id}-code-storage"
  path_to_function_code = "../../../src/pubsub"
  company_prefix        = random_string.company_prefix.id
  function_name         = random_pet.main.id
  project_id            = var.project_id
  function_runtime      = "python39"
  service_account_email = var.service_account_email
  entry_point           = "hello_world"
  environment_variables = local.environment_variables
  pub_sub_name          = random_pet.pubsub.id
}
