/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
output "http_function_names" {
  value       = module.example.http_function_names
  description = "The names of the http functions that have been created"
}

output "pubsub_function_names" {
  value       = module.example.pubsub_function_names
  description = "The names of the pubsub functions that have been created"
}
output "function_region" {
  value       = module.example.function_region
  description = "The region of the functions that's been created"
}
output "project_id" {
  description = "The ID of the project in which resources are provisioned."
  value       = var.project_id
}
