/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

# provider "random" {
#   version = "~> 2.0"
# }

locals {
  function_stack_http = {
    "hello" = {
      "function_name"         = "${random_pet.http.id}-hello",
      "entry_point"           = "hello_world",
      "environment_variables" = { "type" : "http" },
    },
    "goodbye" = {
      "function_name"         = "${random_pet.http.id}-goodbye",
      "entry_point"           = "goodbye_world",
      "environment_variables" = { "type" : "http" },
    }
  }
  function_stack_pubsub = {
    "hello" = {
      "function_name"         = "${random_pet.pubsub.id}-hello",
      "entry_point"           = "hello_world",
      "environment_variables" = { "type" : "pubsub" },
      "pub_sub_name"          = "${random_pet.pubsub.id}"
    }
  }
}

resource "random_string" "company_prefix" {
  length  = 3
  lower   = true
  special = false
  number  = false
  upper   = false
}

resource "random_pet" "http" {
  length    = 2
  prefix    = "http"
  separator = "-"
}
resource "random_pet" "pubsub" {
  length    = 2
  prefix    = "pubsub"
  separator = "-"
}

locals {
  environment_variables = { "gcp_project_id" : var.project_id }
}

module "example" {
  source                = "../../../examples/stack"
  bucket_name           = "${var.project_id}-code-storage"
  path_to_function_code = "../../../src/stack"
  company_prefix        = random_string.company_prefix.id
  project_id            = var.project_id
  service_account_email = var.service_account_email
  environment_variables = local.environment_variables
  function_stack_http   = local.function_stack_http
  function_stack_pubsub = local.function_stack_pubsub
}
