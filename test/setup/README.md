<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| gcs\_location | The location any storage will be made in | `string` | `"australia-southeast1"` | no |
| project\_id | the name of the project - premade to deploy in | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| project\_id | n/a |
| sa\_key | n/a |
| service\_account\_email | The email of the generated test email we will be using |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->