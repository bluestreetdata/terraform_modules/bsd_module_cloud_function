/**
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

data "google_project" "kitchen_project" {
  project_id = var.project_id
}

resource "random_pet" "main" {
  length    = 1
  prefix    = "frankie"
  separator = "-"
}

locals {
  cf_basic_roles = [
    "roles/run.invoker",
    "roles/pubsub.publisher",
    "roles/pubsub.subscriber"
  ]

}

resource "google_service_account" "service_account" {
  account_id   = random_pet.main.id
  display_name = random_pet.main.id
  project      = var.project_id
}

# resource "random_string" "bucket_name" {
#   length  = 10
#   lower   = true
#   special = false
#   number  = false
#   upper   = false
# }

# resource "google_storage_bucket" "code_bucket" {
#   project       = var.project_id
#   location      = var.gcs_location
#   storage_class = "STANDARD"
#   force_destroy = true
#   name          = random_string.bucket_name.id
# }
