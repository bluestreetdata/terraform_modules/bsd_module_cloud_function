# copyright: 2018, The Authors
title 'Verifying the cloud storage has been set up correctly'

FUNCTION_NAME = attribute('function_name',description: 'the name of the created function')
FUNCTION_REGION = attribute('function_region',description: 'the region the function was created in')
TOPIC_NAME = attribute('pubsub_topic',description: 'the name of the topic')
control "pubsub-function" do
 title 'The pubsub function has been created.'

  describe google_cloudfunctions_cloud_function(project: attribute("project_id"), name: FUNCTION_NAME, location: FUNCTION_REGION) do
    it { should exist }
    its('entry_point') { should eq 'hello_world' }
  end
end

control "pubsub-topic" do
  title 'The pubsub function has been created.'
  describe google_pubsub_topic(project: attribute("project_id"), name: TOPIC_NAME) do
    it { should exist }
  end
end