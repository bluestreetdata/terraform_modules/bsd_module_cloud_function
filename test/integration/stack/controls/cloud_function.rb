# copyright: 2018, The Authors
title 'Verifying the stack of functions have been created'

HTTP_FUNCTION_NAMES = attribute('http_function_names',description: 'the names of the created function')
PUBSUB_FUNCTION_NAMES = attribute('pubsub_function_names',description: 'the names of the created function')
FUNCTION_REGION = attribute('function_region',description: 'the region the function was created in')
control "http-stack" do
	title 'The http functions that have been created.'
  	HTTP_FUNCTION_NAMES.each do |function_name|
    	describe google_cloudfunctions_cloud_function(project: attribute("project_id"), name: function_name, location: FUNCTION_REGION) do
        	it { should exist }
			its('environment_variables') { should include('gcp_project_id' => attribute("project_id")) }
			its('environment_variables') { should include('type' => 'http') }  
    	end
  	end
end

control "pubsub-stack" do
	title 'The pubsub functions that have been created.'
	PUBSUB_FUNCTION_NAMES.each do |function_name|
    	describe google_cloudfunctions_cloud_function(project: attribute("project_id"), name: function_name, location: FUNCTION_REGION) do
        	it { should exist }
			its('environment_variables') { should include('gcp_project_id' => attribute("project_id")) }
			its('environment_variables') { should include('type' => 'pubsub') }  
    	end
  	end
end