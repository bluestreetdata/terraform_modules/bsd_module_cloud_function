# copyright: 2018, The Authors
title 'Verifying the cloud storage has been set up correctly'

FUNCTION_NAME = attribute('function_name',description: 'the name of the created data bucket')
FUNCTION_REGION = attribute('function_region',description: 'the name of the created code bucket')

control "http-function" do
 title 'The http function has been created.'

  describe google_cloudfunctions_cloud_function(project: attribute("project_id"), name: FUNCTION_NAME, location: FUNCTION_REGION) do
    it { should exist }
    its('entry_point') { should eq 'hello_world' }
  end
end
