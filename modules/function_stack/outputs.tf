output "http_function_names" {
  value = [
    for function_name in module.http-stack : function_name.function_name
  ]
  description = "The names of the http functions"
}

output "pubsub_function_names" {
  value = [
    for function_name in module.pubsub-stack : function_name.function_name
  ]
  description = "The names of the pubsub functions"
}

output "function_region" {
  value       = var.region
  description = "The region of the function that's been created"
}
