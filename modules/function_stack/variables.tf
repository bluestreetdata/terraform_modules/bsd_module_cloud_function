# https://www.terraform.io/language/expressions/type-constraints#experimental-optional-object-type-attributes
terraform {
  experiments = [module_variable_optional_attrs]
}
variable "project_id" {
  description = "The project in which the function will be created"
}
variable "bucket_name" {
  type        = string
  description = "The name of the bucket to save the compressed file too"

}
variable "path_to_function_code" {
  type        = string
  description = "The relative path (from terraform code) to the diretory of the cloud function code"

}

variable "company_prefix" {
  type        = string
  description = "the unique prefix for resource naming"
}
variable "environment_variables" {
  type        = map(any)
  description = "Map of key value pairs to be assigned to the environment"
  default     = {}

}
variable "service_account_email" {
  type        = string
  description = "The name of the service account to run the function"

}
variable "region" {
  description = "The location of the function"
  default     = "australia-southeast1"

}
variable "labels" {
  type        = map(any)
  description = "labels to be applied"
  default     = {}

}
variable "function_runtime" {
  type        = string
  default     = "python39"
  description = "The runtime for the function"

}
variable "files_to_exclude_in_source_dir" {
  type        = list(string)
  default     = []
  description = "Specify files to ignore when reading the source_dir"
}
variable "function_stack_http" {
  default = {
  }
  type = map(object({
    entry_point           = string
    function_name         = string
    function_description  = optional(string)
    timeout               = optional(number)
    environment_variables = optional(map(any))
    available_memory_mb   = optional(number)
  }))
  description = "An object that sets `entry_point` `function_name` optionally :`timeout`, `function_description` and a map of `environment_variables`"

}
variable "function_stack_pubsub" {
  default = {
  }
  type = map(object({
    entry_point           = string
    function_name         = string
    function_description  = optional(string)
    timeout               = optional(number)
    pub_sub_name          = string
    environment_variables = optional(map(any))
    available_memory_mb   = optional(number)
  }))
  description = "An object that sets `entry_point` `function_name`  `pub_sub_name`, optionally: `timeout`, `function_description` and a map of `environment_variables`"

}
