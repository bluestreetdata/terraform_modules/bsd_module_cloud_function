# Function Stack
This will create a combination of functions that share the same source code
## Usage

Basic usage of this module is as follows:

```hcl
module "bsd_cloud_function_stack" {
  source         = "git::https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_cloud_function.git//modules/stack?ref=tags/v0.0.0"
  project_id     = "<PROJECT ID>"
  company_prefix = "<COMPANY PREFIX>"
  
  sa_name        = "name to describe here"
}
```

Functional examples are included in the
[examples](./examples/) directory.


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| bucket\_name | The name of the bucket to save the compressed file too | `string` | n/a | yes |
| company\_prefix | the unique prefix for resource naming | `string` | n/a | yes |
| environment\_variables | Map of key value pairs to be assigned to the environment | `map(any)` | `{}` | no |
| files\_to\_exclude\_in\_source\_dir | Specify files to ignore when reading the source\_dir | `list(string)` | `[]` | no |
| function\_runtime | The runtime for the function | `string` | `"python39"` | no |
| function\_stack\_http | An object that sets `entry_point` `function_name` optionally :`timeout`, `function_description` and a map of `environment_variables` | <pre>map(object({<br>    entry_point           = string<br>    function_name         = string<br>    function_description  = optional(string)<br>    timeout               = optional(number)<br>    environment_variables = optional(map(any))<br>    available_memory_mb   = optional(number)<br>  }))</pre> | `{}` | no |
| function\_stack\_pubsub | An object that sets `entry_point` `function_name`  `pub_sub_name`, optionally: `timeout`, `function_description` and a map of `environment_variables` | <pre>map(object({<br>    entry_point           = string<br>    function_name         = string<br>    function_description  = optional(string)<br>    timeout               = optional(number)<br>    pub_sub_name          = string<br>    environment_variables = optional(map(any))<br>    available_memory_mb   = optional(number)<br>  }))</pre> | `{}` | no |
| labels | labels to be applied | `map(any)` | `{}` | no |
| path\_to\_function\_code | The relative path (from terraform code) to the diretory of the cloud function code | `string` | n/a | yes |
| project\_id | The project in which the function will be created | `any` | n/a | yes |
| region | The location of the function | `string` | `"australia-southeast1"` | no |
| service\_account\_email | The name of the service account to run the function | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| function\_region | The region of the function that's been created |
| http\_function\_names | The names of the http functions |
| pubsub\_function\_names | The names of the pubsub functions |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

