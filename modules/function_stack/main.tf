locals {
  labels = {
    tf : "cloud_function"

  }
}
module "archive-code" {
  source                         = "../archive"
  bucket_name                    = var.bucket_name
  path_to_function_code          = var.path_to_function_code
  company_prefix                 = var.company_prefix
  files_to_exclude_in_source_dir = var.files_to_exclude_in_source_dir
}

module "http-stack" {
  source                = "../http"
  for_each              = var.function_stack_http
  function_name         = each.value.function_name
  function_description  = each.value.function_description
  region                = var.region
  project_id            = var.project_id
  function_runtime      = var.function_runtime
  bucket_name           = var.bucket_name
  source_archive_object = module.archive-code.source_archive_object
  service_account_email = var.service_account_email
  entry_point           = each.value.entry_point
  timeout               = each.value.timeout
  labels                = merge(var.labels, local.labels)
  environment_variables = merge(var.environment_variables, each.value.environment_variables)
  available_memory_mb   = each.value.available_memory_mb

}

module "pubsub-stack" {
  source                = "../pubsub"
  for_each              = var.function_stack_pubsub
  function_name         = each.value.function_name
  function_description  = each.value.function_description
  region                = var.region
  project_id            = var.project_id
  pub_sub_name          = each.value.pub_sub_name
  function_runtime      = var.function_runtime
  bucket_name           = var.bucket_name
  source_archive_object = module.archive-code.source_archive_object
  service_account_email = var.service_account_email
  entry_point           = each.value.entry_point
  timeout               = each.value.timeout
  labels                = merge(var.labels, local.labels)
  environment_variables = merge(var.environment_variables, each.value.environment_variables)
  available_memory_mb   = each.value.available_memory_mb

}
