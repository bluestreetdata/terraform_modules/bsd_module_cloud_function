variable "bucket_name" {
  type        = string
  description = "The name of the bucket to save the compressed file too"

}
variable "path_to_function_code" {
  type        = string
  description = "The relative path (from terraform code) to the diretory of the cloud function code"

}


variable "company_prefix" {
  type        = string
  description = "the unique prefix for resource naming"
}

variable "files_to_exclude_in_source_dir" {
  type        = list(string)
  default     = []
  description = "Specify files to ignore when reading the source_dir"
}
