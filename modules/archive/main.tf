# resource "null_resource" "dependent_files" {
#   triggers = {
#     # build_number = timestamp()
#     for file in var.source_dependent_files :
#     pathexpand(file.filename) => file.id
#   }
# }

data "null_data_source" "wait_for_files" {
  inputs = {
    # This ensures that this data resource will not be evaluated until
    # after the null_resource has been created.
    # dependent_files_id = null_resource.dependent_files.id

    # This value gives us something to implicitly depend on
    # in the archive_file below.
    source_dir = pathexpand(var.path_to_function_code)
  }
}

data "archive_file" "main" {
  type        = "zip"
  output_path = pathexpand("${var.path_to_function_code}.zip")
  source_dir  = data.null_data_source.wait_for_files.outputs["source_dir"]
  excludes    = var.files_to_exclude_in_source_dir
}

resource "google_storage_bucket_object" "function-archive" {
  name                = "${var.company_prefix}_functions/${data.archive_file.main.output_md5}-${basename(data.archive_file.main.output_path)}"
  bucket              = var.bucket_name
  source              = data.archive_file.main.output_path
  content_disposition = "attachment"
  content_encoding    = "gzip"
  content_type        = "application/zip"
}

