output "source_archive_object" {
  value       = google_storage_bucket_object.function-archive.name
  description = "The name of the object saved to gcs"
}
