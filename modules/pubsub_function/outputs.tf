output "function_name" {
  value       = module.trigger-function.function_name
  description = "The name of the function that's been created"
}
output "function_region" {
  value       = module.trigger-function.function_region
  description = "The region of the function that's been created"
}
output "pubsub_topic" {
  value       = module.trigger-function.pubsub_topic
  description = "The name of the created pub/sub topic"

}
