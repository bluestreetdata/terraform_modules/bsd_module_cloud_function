locals {
  labels = {
    tf : "cloud_function"

  }
}
module "archive-code" {
  source                         = "../archive"
  bucket_name                    = var.bucket_name
  path_to_function_code          = var.path_to_function_code
  company_prefix                 = var.company_prefix
  files_to_exclude_in_source_dir = var.files_to_exclude_in_source_dir
}


module "trigger-function" {
  source                = "../pubsub"
  function_name         = var.function_name
  function_description  = var.function_description
  region                = var.region
  pub_sub_name          = var.pub_sub_name
  project_id            = var.project_id
  function_runtime      = var.function_runtime
  bucket_name           = var.bucket_name
  source_archive_object = module.archive-code.source_archive_object
  service_account_email = var.service_account_email
  entry_point           = var.entry_point
  timeout               = var.timeout
  labels                = merge(var.labels, local.labels)
  environment_variables = var.environment_variables
  available_memory_mb   = var.available_memory_mb

}
