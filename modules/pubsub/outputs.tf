output "function_name" {
  value       = google_cloudfunctions_function.trigger-function.name
  description = "The name of the function that's been created"
}


output "function_region" {
  value       = google_cloudfunctions_function.trigger-function.region
  description = "The region of the function that's been created"
}

output "pubsub_topic" {
  value       = google_pubsub_topic.trigger-topic.name
  description = "The name of the created pub/sub topic"

}
