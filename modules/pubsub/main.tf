locals {
  labels = {
    tf : "cloud_function"

  }
}


resource "google_pubsub_topic" "trigger-topic" {
  project = var.project_id
  name    = var.pub_sub_name
}



resource "google_cloudfunctions_function" "trigger-function" {
  name                  = var.function_name
  description           = var.function_description
  region                = var.region
  project               = var.project_id
  runtime               = var.function_runtime
  source_archive_bucket = var.bucket_name
  source_archive_object = var.source_archive_object
  service_account_email = var.service_account_email
  event_trigger {
    event_type = "providers/cloud.pubsub/eventTypes/topic.publish"
    resource   = google_pubsub_topic.trigger-topic.name
  }
  entry_point           = var.entry_point
  timeout               = var.timeout
  labels                = merge(var.labels, local.labels)
  environment_variables = var.environment_variables
  available_memory_mb   = var.available_memory_mb
}
