output "function_name" {
  value       = google_cloudfunctions_function.http-function.name
  description = "The name of the function that's been created"
}


output "function_region" {
  value       = google_cloudfunctions_function.http-function.region
  description = "The region of the function that's been created"
}

output "function_entry_point" {
  value       = google_cloudfunctions_function.http-function.entry_point
  description = "The entry point of the function that's been created"
}

output "function_environment_variables" {
  value       = google_cloudfunctions_function.http-function.environment_variables
  description = "The list of env vars for the created function"
}
