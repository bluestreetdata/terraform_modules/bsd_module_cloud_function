locals {
  labels = {
    tf : "cloud_function"

  }
}


resource "google_cloudfunctions_function" "http-function" {
  name                  = var.function_name
  description           = var.function_description
  region                = var.region
  project               = var.project_id
  runtime               = var.function_runtime
  source_archive_bucket = var.bucket_name
  source_archive_object = var.source_archive_object
  service_account_email = var.service_account_email
  trigger_http          = true
  entry_point           = var.entry_point
  timeout               = var.timeout
  labels                = merge(var.labels, local.labels)
  environment_variables = var.environment_variables
  available_memory_mb   = var.available_memory_mb
}
