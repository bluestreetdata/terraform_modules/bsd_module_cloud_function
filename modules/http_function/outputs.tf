output "function_name" {
  value       = module.http-function.function_name
  description = "The name of the function that's been created"
}


output "function_region" {
  value       = module.http-function.function_region
  description = "The region of the function that's been created"
}
