# Changelog

All notable changes to this project will be documented in this file.

The format is based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

## [0.0.1](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_cloud_function/-/releases/v0.0.1) - 2022-04-07

### Added

- Http triggered function
- Pub/Sub Triggered functions
- Function stack

