"""
_summary_
"""

def hello_world(request):
    """
    _summary_

    Args:
        request (_type_): _description_

    Returns:
        _type_: _description_
    """
    print(request)
    return {"hello": "world"}


def goodbye_world(request):
    """
    _summary_

    Args:
        request (_type_): _description_

    Returns:
        _type_: _description_
    """
    print(request)
    return {"goodbye": "world"}


def hello_pubsub(event, context):
    """
    _summary_

    Args:
        event (_type_): _description_
        context (_type_): _description_
    """
    print(event)
    print(context)
